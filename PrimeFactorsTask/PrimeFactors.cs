﻿using System;

#pragma warning disable 

namespace PrimeFactorsTask
{
    public static class PrimeFactors
    {
        /// <summary>
        /// Compute the prime factors of a given natural number.
        /// A prime number is only evenly divisible by itself and 1.
        /// Note that 1 is not a prime number.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <returns>Prime factors of a given natural number.</returns>
        /// <exception cref="ArgumentException">Thrown when number less or equal 0.</exception>
        /// <example>
        /// 60 => {2, 2, 3, 5}
        /// 8 => {2, 2, 2}
        /// 12 => {2, 2, 3}
        /// 901255 => {5, 17, 23, 461}
        /// 93819012551 => {11, 9539, 894119}
        /// </example>

        public static bool IsPrime(int number)
        {
            for (int i = 2; i * i <= number; i++)
            {
                if (number % i == 0) return false;
            }

            return true;
        }
        public static int[] GetFactors(int number)
        {
            if (number <= 0) throw new ArgumentException(string.Empty, nameof(number));

            int[] result =new int[number];
            int count = 0;
            while (number != 1)
            {
                for (int i=2;i<=number;i++)
                {
                    if(number % i == 0 && IsPrime(i))
                    {
                        result[count] = i;                   
                        break;
                    }
                }
                number /= result[count];
                count++;
            }
            int[] ans = new int[count];
            for(int i=0;i<count;i++)
            {
                ans[i]=result[i];
            }
            return ans;
        }

       
    }
}
